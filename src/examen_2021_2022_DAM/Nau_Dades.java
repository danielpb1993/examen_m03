package examen_2021_2022_DAM;

import java.util.ArrayList;
import java.util.List;

public class Nau_Dades {
    private String nau_ID;
    private String nau_nom;
    private List<Peça_electronica_Dades> llistaPecesElectronica = new ArrayList<Peça_electronica_Dades>();
    private List<Peça_mampara_Dades> llistaPecesMampara = new ArrayList<Peça_mampara_Dades>();

    public Nau_Dades(String pID, String pNom) {
        this.nau_ID = pID;
        this.nau_nom = pNom;
    }

    public String getNau_ID() {
        return nau_ID;
    }

    public void setNau_ID(String nau_ID) {
        this.nau_ID = nau_ID;
    }

    public String getNau_nom() {
        return nau_nom;
    }

    public void setNau_nom(String nau_nom) {
        this.nau_nom = nau_nom;
    }

    public List<Peça_electronica_Dades> getLlistaPecesElectronica() {
        return llistaPecesElectronica;
    }

    public void setLlistaPecesElectronica(List<Peça_electronica_Dades> llistaPecesElectronica) {
        this.llistaPecesElectronica = llistaPecesElectronica;
    }

    public List<Peça_mampara_Dades> getLlistaPecesMampara() {
        return llistaPecesMampara;
    }

    public void setLlistaPecesMampara(List<Peça_mampara_Dades> llistaPecesMampara) {
        this.llistaPecesMampara = llistaPecesMampara;
    }
}
