package examen_2021_2022_DAM;

public class Peça_electronica_Dades extends Peça_prototipus_Dades {
    private int peça_consumEnergetic;
    private String peça_consumEnergetic_Unitat_de_mesura;

    public Peça_electronica_Dades(String peça_ID, int peça_num_serie, String peça_nom, String fabricant_ID, boolean peça_reparable, boolean peça_trencada,
                                  int peça_consumEnergetic, String peça_consumEnergetic_Unitat_de_mesura) {
        super(peça_ID, peça_num_serie, peça_nom, fabricant_ID, peça_reparable, peça_trencada);
        this.peça_consumEnergetic = peça_consumEnergetic;
        this.peça_consumEnergetic_Unitat_de_mesura = peça_consumEnergetic_Unitat_de_mesura;
    }

    public int getPeça_consumEnergetic() {
        return peça_consumEnergetic;
    }

    public void setPeça_consumEnergetic(int peça_consumEnergetic) {
        this.peça_consumEnergetic = peça_consumEnergetic;
    }

    public String getPeça_consumEnergetic_Unitat_de_mesura() {
        return peça_consumEnergetic_Unitat_de_mesura;
    }

    public void setPeça_consumEnergetic_Unitat_de_mesura(String peça_consumEnergetic_Unitat_de_mesura) {
        this.peça_consumEnergetic_Unitat_de_mesura = peça_consumEnergetic_Unitat_de_mesura;
    }

    @Override
    boolean esReparable() {
        return isPeça_reparable();
    }
}
