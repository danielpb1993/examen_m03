package examen_2021_2022_DAM;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;

public class Drassana_Dades {
    protected static final String nomDrassana = "MRCN Calisto";
//    private List<Nau_Dades> llistaNausEnDrassana;
    private List<Nau_Dades> llistaNausEnDrassana = new LinkedList<Nau_Dades>();
    //private SortedMap<> mapaStockPeces;
    private LinkedHashMap<Integer, Peça_electronica_Dades> mapaPecesElectronica;
    private LinkedHashMap<Integer, Peça_mampara_Dades> mapaPecesMampares;

    public List<Nau_Dades> getLlistaNausEnDrassana() {
        return llistaNausEnDrassana;
    }

    public void setLlistaNausEnDrassana(List<Nau_Dades> llistaNausEnDrassana) {
        this.llistaNausEnDrassana = llistaNausEnDrassana;
    }

    public LinkedHashMap<Integer, Peça_electronica_Dades> getMapaPecesElectronica() {
        return mapaPecesElectronica;
    }

    public void setMapaPecesElectronica(LinkedHashMap<Integer, Peça_electronica_Dades> mapaPecesElectronica) {
        this.mapaPecesElectronica = mapaPecesElectronica;
    }

    public LinkedHashMap<Integer, Peça_mampara_Dades> getMapaPecesMampares() {
        return mapaPecesMampares;
    }

    public void setMapaPecesMampares(LinkedHashMap<Integer, Peça_mampara_Dades> mapaPecesMampares) {
        this.mapaPecesMampares = mapaPecesMampares;
    }
}
