package examen_2021_2022_DAM;


import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Drassana {
	
	//1. Inicialitzar naus (part I)
	public static void inicialitzarNaus(Drassana_Dades drassana_MCRN) {
		Nau_Dades donnager = new Nau_Dades("mcrn 101", "Donnager");
		Nau_Dades pella = new Nau_Dades("mcrn 202", "Pella");
		Nau_Dades carcassone = new Nau_Dades("mcrn 303", "Carcassone");

		drassana_MCRN.getLlistaNausEnDrassana().add(donnager);
		drassana_MCRN.getLlistaNausEnDrassana().add(pella);
		drassana_MCRN.getLlistaNausEnDrassana().add(0, carcassone);

		for (Nau_Dades nau_dadesTmp : drassana_MCRN.getLlistaNausEnDrassana()) {
			System.out.println("Drassana " + Drassana_Dades.nomDrassana + ": AFEGIDA LA NAU " + nau_dadesTmp.getNau_nom() + " AMB ID " + nau_dadesTmp.getNau_ID());
		}
	}

	
	//1. Inicialitzar naus (part II)
	public static void inicialitzarPecesNaus(Drassana_Dades drassana_MCRN) {

		Nau_Dades nau_dadesTmp;

		List<Peça_electronica_Dades> llista_radars = new ArrayList<Peça_electronica_Dades>();
		//Peces_electroniques de tipus radar:
		Peça_electronica_Dades peçaERadar1 = new Peça_electronica_Dades("Cyrano IV", 0, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, true, 120, "kW");
		Peça_electronica_Dades peçaERadar2 = new Peça_electronica_Dades("Cyrano IV", 1, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", false, true, 120, "kW");
		Peça_electronica_Dades peçaERadar3 = new Peça_electronica_Dades("Cyrano IV", 2, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");

		llista_radars.add(peçaERadar1);
		llista_radars.add(peçaERadar2);
		llista_radars.add(peçaERadar3);

		List<Peça_electronica_Dades> llista_visorsIR = new ArrayList<Peça_electronica_Dades>();
//		Peces_electroniques de tipus visorIR:
		Peça_electronica_Dades peçaEVisor1 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 0, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		Peça_electronica_Dades peçaEVisor2 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 1, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, true, 5, "kW");
		Peça_electronica_Dades peçaEVisor3 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 2, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");

		llista_visorsIR.add(peçaEVisor1);
		llista_visorsIR.add(peçaEVisor2);
		llista_visorsIR.add(peçaEVisor3);

		List<Peça_mampara_Dades> llista_mampares = new ArrayList<Peça_mampara_Dades>();
//		Peces mampara de tipus mampara5x5:
		Peça_mampara_Dades peçaMampara1= new Peça_mampara_Dades("mampara de 5x5", 0, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		Peça_mampara_Dades peçaMampara2 = new Peça_mampara_Dades("mampara de 5x5", 1, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, true, 5, 5);
		Peça_mampara_Dades peçaMampara3 = new Peça_mampara_Dades("mampara de 5x5", 2, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);

		llista_mampares.add(peçaMampara1);
		llista_mampares.add(peçaMampara2);
		llista_mampares.add(peçaMampara3);

		ListIterator<Nau_Dades> listIt = drassana_MCRN.getLlistaNausEnDrassana().listIterator(drassana_MCRN.getLlistaNausEnDrassana().size());

		while (listIt.hasPrevious()) {
			nau_dadesTmp = listIt.previous();
			System.out.println("Nau " + nau_dadesTmp.getNau_nom() + " (ID " + nau_dadesTmp.getNau_ID() + "):");

//			for (int i = 0; i < llista_radars.size(); i++) {
//				nau_dadesTmp.getLlistaPecesElectronica().add(llista_radars(i));
//			}

			for (Peça_electronica_Dades peçaRadarTmp : llista_radars) {
				nau_dadesTmp.getLlistaPecesElectronica().add(peçaRadarTmp);
				System.out.println("\tAFEGIDES LES PECES ELECTRÒNIQUES: ");
				System.out.println("\t\t" + peçaRadarTmp.getPeça_nom() + " Nº SERIE " + peçaRadarTmp.getPeça_num_serie());
			}

			for (Peça_electronica_Dades peçaVisorTmp : llista_visorsIR) {
				nau_dadesTmp.getLlistaPecesElectronica().add(peçaVisorTmp);
				System.out.println("\tAFEGIDES LES PECES ELECTRÒNIQUES: ");
				System.out.println("\t\t" + peçaVisorTmp.getPeça_nom() + " Nº SERIE " + peçaVisorTmp.getPeça_num_serie());
			}

			for (Peça_mampara_Dades peçaMamparaTmp : llista_mampares) {
				nau_dadesTmp.getLlistaPecesMampara().add(peçaMamparaTmp);
				System.out.println("\tAFEGIDES LES PECES ELECTRÒNIQUES: ");
				System.out.println("\t\t" + peçaMamparaTmp.getPeça_nom() + " Nº SERIE " + peçaMamparaTmp.getPeça_num_serie());
			}
		}
	}
	

	//2. Inicialitzar drassana
	public static void inicialitzarPecesDrassana(Drassana_Dades drassana_MCRN) {
		/*
		Peces_electroniques de tipus radar: 
		("Cyrano IV", 3, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, true, 120, "kW");
		("Cyrano IV", 4, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", false, true, 120, "kW");
		("Cyrano IV", 5, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		("Cyrano IV", 6, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		("Cyrano IV", 7, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		
		
		Peces_electroniques de tipus radio: 
		("radio UHF/VHF", 0, "radio salto frecuencias UHF/VHF", "Thomson-CSF", true, false, 10, "kW");
		("radio UHF/VHF", 1, "radio salto frecuencias UHF/VHF", "Thomson-CSF", true, false, 10, "kW");
		
		
		Peces_electroniques de tipus visorIR: 
		("SAT SCM2400 Super Cyclone", 0, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		("SAT SCM2400 Super Cyclone", 1, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		("SAT SCM2400 Super Cyclone", 2, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		
		
		Peces mampara de tipus mampara5x5:
		("mampara de 5x5", 0, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		("mampara de 5x5", 1, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		*/
	}
	
	
	//10. Drassana: veure stock de peces
	// Veure el nº de peces de repost que hi ha en la drassana.
	// Fer servir un iterador per les claus del mapa.
	public static void veureStockPecesEnDrassana(Drassana_Dades drassana_MCRN) {

	}
	
}
